#ifndef GAMETREE_H
#define GAMETREE_H

#include <QList>
#include <gameforsolver.h>

class Node
{
public:
    Node(GameForSolver game);
    ~Node();
    int fittness();
    QList<Node*> children;
    void setOperationOnGame(const char direction);
    char operation() const { return _operation; }
    void expand();

private:
    GameForSolver game;
    char _operation;
    bool goodMove;
};


class GameTree
{
public:
    GameTree(GameForSolver game, int maxDeepness);
    ~GameTree();
    char getBest();
    QList< QPair<char, int> > getOperationScores();

private:
    Node *root;
};

#endif // GAMETREE_H
