#include "space.h"

#include <iostream>
#include <QDebug>

/**
 * Constructor: Creates free space for cells
 * @param width
 * @param height
 */
Space::Space(int width, int height)
    : width(width), height(height)
{
    space = new Cell**[height];
    for(int i = 0; i<height; i++) {
        space[i] = new Cell*[width];
        for(int j = 0; j<width; j++) {
           space[i][j] = NULL;
        }
    }
}


/**
 * Destructor: deletes all the cells in the space,
 * and deletes the space itself
 */
Space::~Space() {
    for(int i = 0; i<height; i++) {
        for(int j = 0; j<width; j++) {
           delete space[i][j];
        }
        delete[] space[i];
    }
    delete[] space;
}


/**
 * Adds a cell to the space. The row and col is relative to the
 * position of the original Cell. This is because the Cell dont know its position.
 *
 * If the position is already occupied then this function tries to place the new Cell
 * randomly in the neighborhood of the original Cell
 *
 * @brief Space::addCell
 * @param original
 * @param cell
 * @param relativeRow
 * @param relativeCol
 * @return true if the placement was success, false if there was no place in the
 *          neighborhood of original
 */
bool Space::addCell(const Cell *original, Cell *cell,
                             int relativeRow, int relativeCol) {
    int row = space_map.value(original).first + relativeRow;
    int col = space_map.value(original).second + relativeCol;
    if(!addCell(cell, row, col))
    { // is the place is occupied or invalid, we try to put somewhere else
        QVector< QPair<int, int> > freeSpace;
        for(int i = -1; i<=1; i++) {
            for(int j = -1; j<=1; j++) {

                if(isValid(row+i, col+j) && !isOccupied(row+i, col+j)) {
                    freeSpace.append(QPair<int, int>(row+i, col+j));
                }
            }
        }
        if(freeSpace.isEmpty()) {
            return false;
        } else {
            int randPos = qrand() % freeSpace.size();
            return addCell(cell, freeSpace.at(randPos).first, freeSpace.at(randPos).second);
        }
    }
    else {
        return true;
    }
}

bool Space::searchMin(const int &number) const {
    for(int i = 0; i<height; i++) {
        for(int j = 0; j<width; j++) {
            if(space[i][j]->getValue() >= number) {
                return true;
            }
        }
    }
    return false;
}


bool Space::addCell(Cell *cell, int row, int col) {
    // out of space boundaries or the position is already occupied
    if(!isValid(row, col) || isOccupied(row, col)) {
        return false;
    }

    space[row][col] = cell;
    space_map.insert(cell, QPair<int, int>(row, col));

    // space connections (grid network)
    Cell *temp = getCell(row-1, col);
    if(temp) {
        temp->down = cell;
        cell->up = temp;
    }

    temp = getCell(row+1, col);
    if(temp) {
        temp->up = cell;
        cell->down = temp;
    }

    temp = getCell(row, col-1);
    if(temp) {
        temp->right = cell;
        cell->left = temp;
    }

    temp = getCell(row, col+1);
    if(temp) {
        temp->left = cell;
        cell->right = temp;
    }

    return true;
}


bool Space::isValid(const int &row, const int &col) const {
    return (row >= 0 && col >= 0 && row < height && col < width);
}


bool Space::isOccupied(const int &row, const int &col) const {
    return (space[row][col] != NULL);
}


Cell *Space::getCell(const int &row, const int &col) {
    if(!isValid(row, col)) {
        return NULL;
    }
    return space[row][col];
}


Cell *Space::getCell(const int &row, const int &col) const {
    if(!isValid(row, col)) {
        return NULL;
    }
    return space[row][col];
}


QList< QPair<int, int> > Space::collectZeroValueCells() const {
    QList< QPair<int, int> > result;
    for(int i = 0; i<height; i++) {
        for(int j = 0; j<width; j++) {
            if(isOccupied(i, j) && space[i][j]->value == 0) {
                result.append(QPair<int, int>(i, j));
            }
        }
    }
    return result;
}
