#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <gameforsolver.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    game = new GameForSolver(4);
    tableModel = game->getGameState();
    ui->tableView->setModel(tableModel);
    for(int i = 0; i<4; i++) {
        ui->tableView->setColumnWidth(i, 60);
    }
    ui->tableView->setFocusPolicy(Qt::NoFocus);

    GameForSolver *s = dynamic_cast<GameForSolver*>(game);
    solver = new Solver(s);
    connect(solver, SIGNAL(step()), this, SLOT(updateTable()));
    solver->start();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()) {
    case Qt::Key_Up:
        game->up(); break;
    case Qt::Key_Down:
        game->down(); break;
    case Qt::Key_Left:
        game->left(); break;
    case Qt::Key_Right:
        game->right(); break;
    }
    updateTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTable()
{
    tableModel->emitDataChanged();
}
