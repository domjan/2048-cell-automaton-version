#ifndef SOLVER_H
#define SOLVER_H

#include <QThread>
#include <gameforsolver.h>

class Solver : public QThread
{
    Q_OBJECT

public:
    Solver(GameForSolver *game);
    void run();

private:
    GameForSolver *game;

signals:
    void step();
};

#endif // SOLVER_H
