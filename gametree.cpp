#include "gametree.h"

Node::Node(GameForSolver game) : game(game) {
}

Node::~Node() {
    foreach(Node *node, children) {
        delete node;
    }
    children.clear();
}

int Node::fittness() {
    if(!goodMove) {
        return -1;
    }

    if(children.length() == 0) {
        return game.fittness();
    }
    else {
        int max = 0;
        foreach(Node *node, children) {
            if(max < node->fittness())
                max = node->fittness();
        }
        return max*0.8+game.fittness()*0.2;
    }
}

void Node::setOperationOnGame(const char direction) {
    bool result = false;
    switch(direction) {
    case 'u':
        result = game.up(); break;
    case 'd':
        result = game.down(); break;
    case 'l':
        result = game.left(); break;
    case 'r':
        result = game.right(); break;
    }
    _operation = direction;
    goodMove = result;
}

void Node::expand() {
    children.append(new Node(game));
    children.last()->setOperationOnGame('u');

    children.append(new Node(game));
    children.last()->setOperationOnGame('d');

    children.append(new Node(game));
    children.last()->setOperationOnGame('l');

    children.append(new Node(game));
    children.last()->setOperationOnGame('r');
}

GameTree::GameTree(GameForSolver game, int maxDeepness) {
    root = new Node(game);
    QList<Node*> nodeToExpand;
    nodeToExpand.append(root);
    for(int i = 0; i<maxDeepness; i++) {
        QList<Node*> expandedNodes;
        foreach(Node *node, nodeToExpand) {
            node->expand();
            expandedNodes.append(node->children);
        }
        nodeToExpand.clear();
        nodeToExpand = expandedNodes;
    }
}

GameTree::~GameTree() {
    delete root;
}

char GameTree::getBest() {
    int max = 0;

    foreach(Node *node, root->children) {
        max = qMax(max, node->fittness());
    }

    foreach(Node *node, root->children) {
        if(max == node->fittness()) {
            return node->operation();
        }
    }
    return 'u';
}

QList< QPair<char, int> > GameTree::getOperationScores()
{
    QList< QPair<char, int> > res;
    foreach(Node *node, root->children) {
        res.append(QPair<char, int>(node->operation(), node->fittness()));
    }
    return res;
}
