#ifndef GAMEFORSOLVER_H
#define GAMEFORSOLVER_H

#include <game.h>

class GameForSolver : public Game
{
    Q_OBJECT

public:
    GameForSolver();
    GameForSolver(int table_size);
    GameForSolver(const GameForSolver &other);
    GameForSolver &operator =(GameForSolver &other);

    int freeCellCount();
    bool isLargestInCorner();

    int fittness();

    int orderIsGood();
};

#endif // GAMEFORSOLVER_H
