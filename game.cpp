#include "game.h"
#include <time.h>
#include <iostream>
#include <QDebug>

/**
 * Constructor, initalizes a Game with a table size.
 * The cells will occupy the available space
 * Puts two random number to the table (initial state)
 *
 * @param table_size
 */
Game::Game(const int &table_size, const int &goal)
    : table_size(table_size), goal(goal), gameStateModel(NULL)
{
    qsrand((unsigned)time(NULL));

    // self organization
    space = new Space(table_size, table_size);
    space->addCell(new Cell(space), 0, 0);
    space->getCell(0, 0)->divide();

    putRandomNumber();
    putRandomNumber();
}

Game::~Game() {
    delete space;
    delete gameStateModel;
}

/**
 * Tell the first row to merge.
 * If there was a state change, then
 * it puts a random number to a free field
 * @return true if the merge was success
 */
bool Game::up() {
    bool result = false;
    for(int i = 0; i<table_size; i++) {
        result |= space->getCell(0, i)->mergeUp();
    }
    if(result)
        putRandomNumber();
    return result;
}


/**
 * Tell the last row to merge.
 * If there was a state change, then
 * it puts a random number to a free field
 * @return true if the merge was success
 */
bool Game::down() {
    bool result = false;
    for(int i = 0; i<table_size; i++) {
        result |= space->getCell(table_size-1, i)->mergeDown();
    }
    if(result)
        putRandomNumber();
    return result;
}

bool Game::left() {
    bool result = false;
    for(int i = 0; i<table_size; i++) {
        result |= space->getCell(i, 0)->mergeLeft();
    }
    if(result)
        putRandomNumber();
    return result;
}

bool Game::right() {
    bool result = false;
    for(int i = 0; i<table_size; i++) {
        result |= space->getCell(i, table_size-1)->mergeRight();
    }
    if(result)
        putRandomNumber();
    return result;
}

/**
 * Puts a 2 or 4 to a random position of the table (to a free place)
 * There is 10% chance that the number will be 4
 */
void Game::putRandomNumber() {
    QList< QPair<int, int> > freeCells = space->collectZeroValueCells();
    if (freeCells.count() > 0) {
        // random cell
        int randCell = qrand() % freeCells.length();

        // 2 or 4 ?
        int number = (qrand() % 100 >= 10) ? 2 : 4;

        // put
        int row = freeCells.at(randCell).first;
        int col = freeCells.at(randCell).second;
        space->getCell(row, col)->setValue(number);
    }
}


/**
 * Returns the actual state
 *
 * @return the actual state of the game. This is a QTableModel which will
 *      contain the actual state at every time instant
 */
GameStateTableModel *Game::getGameState()
{
    if(gameStateModel == NULL) {
        gameStateModel = new GameStateTableModel(this);
        gameStateModel->setSpace(space);
    }
    return gameStateModel;
}
