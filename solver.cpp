#include "solver.h"
#include <QApplication>
#include <gametree.h>

Solver::Solver(GameForSolver *game) : game(game)
{
}


void Solver::run()
{
    while(true) {

        GameTree tree(*game, 4);
        QList< QPair<char, int> > scores1 = tree.getOperationScores();
        GameTree tree2(*game, 4);
        QList< QPair<char, int> > scores2 = tree2.getOperationScores();

        QList< QPair<char, double> > finalScores;
        for(int i = 0; i<4; i++) {
            finalScores.append(
                        QPair<char, double>(
                            scores1.at(i).first,
                            (double)(
                                qMin(scores1.at(i).second, scores2.at(i).second)
                                )));
        }

        double max = 0;
        for(int i = 0; i<4; i++) {
            max = qMax(max, finalScores.at(i).second);
        }

        char dir;
        for(int i = 0; i<4; i++) {
            if(max == finalScores.at(i).second) {
                dir = finalScores.at(i).first;
            }
        }

        if(max == 0) {
            if(game->isWin()) {
                qDebug()<<"Win";
                qApp->exit(1);
            }
            else {
                qDebug()<<"Lose";
                qApp->exit(0);
            }
        }


        switch(dir) {
        case 'u':
            game->up(); break;
        case 'd':
            game->down(); break;
        case 'l':
            game->left(); break;
        case 'r':
            game->right(); break;
        }

        usleep(50);
        emit step();
    }
}
