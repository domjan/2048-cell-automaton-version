#ifndef SPACE_H
#define SPACE_H

#include <cell.h>
#include <QMap>
#include <QPair>

class Space
{
public:
    Space(int width, int height);
    ~Space();
    bool addCell(Cell *cell, int row, int col);
    bool addCell(const Cell *original, Cell *cell,
                                 int relativeRow, int relativeCol);
    Cell *getCell(const int &row, const int &col);
    Cell *getCell(const int &row, const int &col) const;

    QList< QPair<int, int> > collectZeroValueCells() const;

    inline int getWidth() const { return width; }
    inline int getHeight() const { return height; }
    bool isOccupied(const int &row, const int &col) const;
    bool isValid(const int &row, const int &col) const;
    bool searchMin(const int &number) const;

private:
    int width, height;
    Cell ***space;
    QMap< const Cell*, QPair<int, int> > space_map;
};

#endif // SPACE_H
