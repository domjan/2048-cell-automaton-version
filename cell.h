#ifndef CELL_H
#define CELL_H

//forward declaration of the Space
class Space;

class Cell
{
public:
    Cell(Space *space);
    void divide() const;
    inline void setValue(const int &value) { this->value = value; }
    inline int getValue() const { return value; }
    inline bool mergeUp() { return mergeFromDirection('d'); }
    inline bool mergeDown() { return mergeFromDirection('u'); }
    inline bool mergeLeft() { return mergeFromDirection('r'); }
    inline bool mergeRight() { return mergeFromDirection('l'); }

private:
    Cell *accessPointer(const char &direction) const;
    bool moveTheFirstNonZeroFromDirection(const char &direction);
    bool mergeFromDirection(const char &direction);

private:
    int value;
    Cell *up, *down, *left, *right;
    Space *space;

    friend class Space;
};

#endif // CELL_H
