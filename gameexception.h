#ifndef GAMEEXCEPTION_H
#define GAMEEXCEPTION_H

#include <QString>
#include <exception>

class GameException : std::exception
{
public:
    GameException(const QString &message) : std::exception(), message(message) { }
    ~GameException() throw () { }
    const char* what() const throw() { return message.toStdString().c_str(); }

private:
    const QString message;
};

#endif // GAMEEXCEPTION_H
