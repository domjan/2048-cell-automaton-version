#ifndef GAME_H
#define GAME_H

#include <QList>
#include <QPair>

#include <space.h>
#include <QObject>
#include <QAbstractTableModel>
#include <QDebug>
#include <QColor>


class GameStateTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    GameStateTableModel(QObject *parent = 0) : QAbstractTableModel(parent), space(NULL)
    { }

    void setSpace(const Space *space) { this->space = space; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const
    { Q_UNUSED(parent); return space->getHeight(); }
    int columnCount(const QModelIndex &parent = QModelIndex()) const
    { Q_UNUSED(parent); return space->getWidth(); }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
        Q_UNUSED(role);
        if(role == Qt::DisplayRole) {
            int value = space->getCell(index.row(), index.column())->getValue();
            return (value == 0) ? QVariant(" ") : QVariant(value);
        }
        else if(role == Qt::BackgroundColorRole) {
            int value = space->getCell(index.row(), index.column())->getValue();
            return QVariant(valueColor(value));
        }
        else if(role == Qt::TextColorRole) {
            int value = space->getCell(index.row(), index.column())->getValue();
            return QVariant(valuefColor(value));
        }
        else
            return QVariant();
    }
    QVariant headerData(int , Qt::Orientation , int role = Qt::DisplayRole) const
    { Q_UNUSED(role); return ""; }

    void emitDataChanged() {
        emit dataChanged(this->index(0, 0), this->index(rowCount()-1, columnCount()-1));
    }

private:
    QColor valueColor(int number) const {
        int i = 0;
        while(number > 1) {
            number /= 2;
            i++;
        }

        int darkness1 = (255.0/11.0)*i;
        int darkness2 = (255.0/4.0)*(i/3);
        return QColor(255-((i%3 == 0) ? darkness1 : darkness2),
                      255-((i%3 == 1) ? darkness1 : darkness2),
                      255-((i%3 == 2) ? darkness1 : darkness2));
    }

    QColor valuefColor(const int &number) const {
        if(number >= 128) {
            return Qt::white;
        } else {
            return Qt::black;
        }
    }

private:
    const Space *space;
};


class Game : public QObject
{
    Q_OBJECT
public:
    Game(const int &table_size = 4, const int &goal = 2048);
    ~Game();
    GameStateTableModel *getGameState();
    inline bool isWin() const { return space->searchMin(goal); }

public slots:
    bool up();
    bool down();
    bool left();
    bool right();

protected:
    int table_size;
    int goal;
    Space *space;

private:
    void putRandomNumber();

private:
    GameStateTableModel *gameStateModel;
};

#endif // GAME_H
