#include "gameforsolver.h"

/**
 * Default constructor
 */
GameForSolver::GameForSolver()
{
}

/**
 * GameForSolver class extends the game class with self-evaluation
 * functions.
 * @param table_size
 */
GameForSolver::GameForSolver(int table_size) : Game(table_size)
{
}

/**
 * CopyConstructor copies the state of the game
 */
GameForSolver::GameForSolver(const GameForSolver &other) {
    delete space;
    space = new Space(table_size, table_size);
    for(int i = 0; i<table_size; i++) {
        for(int j = 0; j<table_size; j++) {
            space->addCell(new Cell(space), i, j);
            space->getCell(i, j)->setValue( other.space->getCell(i, j)->getValue() );
        }
    }
}

GameForSolver &GameForSolver::operator =(GameForSolver &other)
{
    delete space;
    space = new Space(table_size, table_size);
    for(int i = 0; i<table_size; i++) {
        for(int j = 0; j<table_size; j++) {
            space->addCell(new Cell(space), i, j);
            space->getCell(i, j)->setValue( other.space->getCell(i, j)->getValue() );
        }
    }
    return *this;
}

int GameForSolver::fittness() {
    int a = isLargestInCorner() ? 10 :0;
    int win = isWin() ? 100 : 0;
    return freeCellCount()*2 + a + orderIsGood() + win;
}

int GameForSolver::freeCellCount() {
    return space->collectZeroValueCells().count();
}

bool GameForSolver::isLargestInCorner() {
    int max = 0;
    for(int i = 0; i<table_size; i++) {
        for(int j = 0; j<table_size; j++) {
            if(space->isOccupied(i, j) && space->getCell(i, j)->getValue() > max) {
                max = space->getCell(i, j)->getValue();
            }
        }
    }

    if(space->getCell(0, 0)->getValue() == max) return true;
    if(space->getCell(0, table_size-1)->getValue() == max) return true;
    if(space->getCell(table_size-1, 0)->getValue() == max) return true;
    if(space->getCell(table_size-1, table_size-1)->getValue() == max) return true;

    return false;
}

int GameForSolver::orderIsGood()
{
    // inside columns
    int count3 = 0;
    for(int i = 0; i<table_size-1; i++) {
        for(int j = 0; j<table_size; j++) {
            if(space->getCell(i, j)->getValue() <= space->getCell(i+1, j)->getValue()) {
                count3+=3;
            }
            else {
                count3-=1;
            }
        }
    }

    // inside rows
    for(int i = 0; i<table_size; i++) {
        for(int j = 0; j<table_size-1; j++) {
            if(space->getCell(i, j)->getValue() >= space->getCell(i, j+1)->getValue()) {
                count3+=2;
            }
            else { //penalty, if the row is not in order
                    count3-=1;
            }
        }
    }

    return count3;
}
