#-------------------------------------------------
#
# Project created by QtCreator 2014-03-21T16:23:55
#
#-------------------------------------------------

QT       += core gui

CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048-solver
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    game.cpp \
    cell.cpp \
    space.cpp \
    gameforsolver.cpp \
    solver.cpp \
    gametree.cpp

HEADERS  += mainwindow.h \
    game.h \
    cell.h \
    space.h \
    gameforsolver.h \
    solver.h \
    gametree.h \
    gameexception.h

FORMS    += mainwindow.ui

INCLUDEPATH += .

QMAKE_CFLAGS=-O3
