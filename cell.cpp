#include "cell.h"
#include <space.h>
#include <gameexception.h>

/**
 * Constructor: initializes private variables
 *
 * @brief Cell::Cell
 * @param space contains cells
 */
Cell::Cell(Space *space)
    : value(0),
      up(NULL), down(NULL), left(NULL), right(NULL),
      space(space)
{ }


/**
 * The Cell divides itself and try to place the new cell arround it.
 * This divide mechanism starts a chain reaction of mitosis.
 */
void Cell::divide() const {
    Cell *sibling = new Cell(space);
    if(space->addCell(this, sibling, 0, 0)) {
        divide();
        sibling->divide(); //genetic material
    } else { //if there is no place the cell dies
        delete sibling;
        sibling = NULL;
    }
}


/**
 * This function returns the Cells up, down, left right pointers
 * Did not want to create macro for that
 * (it would be faster with macro, but less understandable)
 *
 * @param direction
 * @returns the up, down, left or right pointer of the cell
 */
Cell *Cell::accessPointer(const char &direction) const {
    switch(direction) {
    case 'u':
        return this->up;
    case 'd':
        return this->down;
    case 'l':
        return this->left;
    case 'r':
        return this->right;
    default:
        throw GameException(
                    QString("Invalid cell pointer access: \"%1\"").arg(direction));
    }
}


/**
 * Move the first non-zero value to a cell.
 * The function finds verticallz or horizontally
 * the first non zero and moves the value to the cell.
 *
 * @param direction can be u(up) d(down) l(left) r(right). That is the direction where
 *          the function will go to find the non-zero
 * @return true if a move was happened
 */
bool Cell::moveTheFirstNonZeroFromDirection(const char &direction) {
    Cell *move_from = this->accessPointer(direction);

    while(move_from && move_from->value == 0) {
        move_from = move_from->accessPointer(direction);
    }

    if(move_from) {
        this->value = move_from->value;
        move_from->value = 0;
        return true;
    }
    else {
        return false;
    }
}


/**
 * This method merges one row or column from a direction
 * For example, Up key merges the four column from Down to Up
 *
 * @param direction can be u(up) d(down) l(left) r(right).
 *          (Pressing "Up" character means merge from "Down")
 * @return true if the merge changed the gamestate (ie. it is possible to merge)
 */
bool Cell::mergeFromDirection(const char &direction) {
    Cell *temp = this->accessPointer(direction);
    bool result = false;
    if(temp) {
        if(this->value == 0) {
            result |= this->moveTheFirstNonZeroFromDirection(direction);
        }

        if(temp->value == 0) {
            result |= temp->moveTheFirstNonZeroFromDirection(direction);
        }

        if(temp->value != 0 && temp->value == this->value) {
            this->value *= 2;
            temp->value = 0;
            result |= true;
        }

        result |= temp->mergeFromDirection(direction);
    }
    return result;
}
